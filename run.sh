#!/bin/sh
echo "\033[1m#-- CONTROL PROGRAMS --#\033[0m\n"
BINS=bin/*
TOTAL=0
PASSED=0

for i in $BINS
do
  TOTAL=$((TOTAL+1))
  $i >/dev/null 2>&1
  if [ $? != 0 ]
  then
    printf "[\033[0;31m N\033[0m ] %s failed\n" "$i"
  else
    printf "[\033[0;32m Y\033[0m ] %s succeeded\n" "$i"
    PASSED=$((PASSED+1))
  fi
done

if [ $((TOTAL)) == $((PASSED)) ]
then
  echo "\033[0;32m"
else
  echo "\033[0;31m"
fi
printf "-- PASSED %d/%d CONTROL PROGRAMS --\n\n" $((PASSED)) $((TOTAL))
echo "\033[0;0m"

TOTAL=0
PASSED=0
echo "\033[1m#-- TEST PROGRAMS --#\033[0m\n"
export LD_PRELOAD=$PWD/lib/libmemalloc.so
for i in $BINS
do
  TOTAL=$((TOTAL+1))
  $i >/dev/null 2>&1
  if [ $? != 0 ]
  then
    printf "[\033[0;31m N\033[0m ] %s failed\n" "$i"
  else
    printf "[\033[0;32m Y\033[0m ] %s succeeded\n" "$i"
    PASSED=$((PASSED+1))
  fi
done

if [ $((TOTAL)) == $((PASSED)) ]
then
  echo "\033[0;32m"
else
  echo "\033[0;31m"
fi
printf "-- PASSED %d/%d TEST PROGRAMS --" $((PASSED)) $((TOTAL))
echo "\033[0;0m"
