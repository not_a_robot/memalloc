#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define MAX_OBJECTS 500
#define NUM_ITERATIONS 50000
#define MAX_SIZE 500
#define MIN_SIZE 50

int
main(void){
  int i, id, size;
  void *allocs[MAX_OBJECTS];

  for(i = 0; i < MAX_OBJECTS; i++)
    allocs[i] = NULL;

  srand(time(NULL));

  for(i = 0; i < NUM_ITERATIONS; i++){
    id = rand() % MAX_OBJECTS;
    size = MIN_SIZE + (rand() % (MAX_SIZE-MIN_SIZE));

    if(allocs[id] != NULL){
      free(allocs[id]);
      allocs[id] = NULL;
    }
    else
      allocs[id] = malloc(size);
  }

  return 0;
}
