#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

#define NUM_ITERATIONS 500
#define BATCH_SIZE 100
#define MAX_SIZE 500
#define MIN_SIZE 50

int
main(void){
  int i, j;

  srand(time(NULL));

  void *batch[BATCH_SIZE];
  for(i = 0; i < NUM_ITERATIONS; i++){
    for(j = 0; j < BATCH_SIZE; j++)
      batch[j] = calloc(1, MIN_SIZE + (rand() % (MAX_SIZE-MIN_SIZE)));
    for(j = 0; j < BATCH_SIZE; j++)
      free(batch[BATCH_SIZE-1-j]);
  }

  return 0;
}
