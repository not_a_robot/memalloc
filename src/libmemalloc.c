#include <sys/mman.h>
#include <stddef.h> /* for NULL */
#include <unistd.h>
#include <string.h>

#include "libmemalloc.h"

#define MAX_DIFF 216

static size_t size_limit = 64, total_size = 0,
              /* reuse blocks while using <75% of threshold */
              size_threshold = 64 * 3 / 4;
header_t *first, *last;

void *find_free(size_t size);
void update_sizes(size_t size);

void *
malloc(size_t size){
  header_t *header;

  if(!size)
    return NULL;

  if((header = find_free(size)))
    return header->occupied = 1, (header_t*)header + 1;
  if((header = mmap(NULL, sizeof(header_t) + size, PROT_WRITE | PROT_READ,
      MAP_SHARED | MAP_ANON, -1, 0)) == MAP_FAILED)
    return NULL;
  header->occupied = 1;
  header->size = size;
  update_sizes(size);

  if(!first){
    first = header;
    first->next = last;
  }
  else if(last)
    last->next = header;
  last = header;
  
  return (header_t*)header + 1;
}

void *
calloc(size_t nmemb, size_t size){
  if(!nmemb || !size)
    return NULL;

  size_t new_size= nmemb * size;
  void *ptr;
  if(new_size / nmemb != size)
    return NULL;

  if(!(ptr = malloc(new_size)))
    return NULL;
  for(unsigned int i = 0; i < nmemb * size; i++)
    *((char *)ptr+i) = 0;
  return ptr;
}

void *
realloc(void *ptr, size_t size){
  header_t *header;

  if(!ptr)
    return malloc(size);
  if(size == ((header_t*)ptr-1)->size)
    return ptr;

  if((header = find_free(size))){ /* move to a smaller block if possible */
    header->occupied = 1;
    memcpy(header+1, ptr, size);
  }
  else{
    header = malloc(size);
    memcpy(header+1, ptr, size);
    free(ptr);
  }

  return header+1;
}

void 
free(void *ptr){
  if(!ptr)
    return;
  header_t *header = (header_t*)ptr - 1;

  /* actually free and remove block from linked list if above threshold */
  if(total_size >= size_threshold){
    header_t *index = first;
    do{
      if(index->next == header){
        index->next = header->next;
        total_size -= header->size;
        if(header == first)
          first = first->next;
        /* malloc checks if last == NULL to organise linked list */
        else if(header == last)
          last = NULL;
        munmap(header, header->size);
      }
    } while((index = index->next));
  }
  else
    header->occupied = 0;
}

/* find_free: find the smallest free block that can fit the size requested */
void *
find_free(size_t size){
  header_t *header = first, *smallest;
  for(; header; header = header->next)
    if(!header->occupied && header->size >= size){
      for(smallest = header; header; header = header->next){
        if(smallest->size == size)
          return smallest;
        if(!header->occupied && header->size < smallest->size)
          smallest = header;
      }
      /* don't waste large blocks for small allocations, create new small
       * allocations instead */
      if(smallest->size - size >= MAX_DIFF)
        break;
      return smallest;
    }
  return NULL;
}

void
update_sizes(size_t size){
  if((total_size += size) >= size_limit){
    while((size_limit <<= 1) < total_size);
    size_threshold = size_limit * 3 / 4;
  }
}
