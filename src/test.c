#include <string.h>
#include <stdlib.h>

int
main(void){
  char *string1, *string2, *string3;
  if(!(string1 = malloc(5)))
    return 1;
  if(!(string2 = malloc(4)))
    return 1;
  strcpy(string1, "1234");
  strcpy(string2, "567");

  free(string1);
  free(string2);

  if(!(string3 = malloc(4)))
    return 1;
  strcpy(string3, "910");
  free(string3);

  return 0;
}
