#ifndef MEMALLOC
#define MEMALLOC
#include <stdint.h>

typedef struct header{
  uint8_t occupied; // uint8_t to be as small as possible; only 1 or 0
  size_t size;
  struct header *next;
} header_t;

void *malloc(size_t size);
void *calloc(size_t nmemb, size_t size);
void *realloc(void *ptr, size_t size);
void free(void *ptr);
#endif
