CC=clang
CFLAGS=-Wall -Wextra -Wpedantic -std=c99 -O3 -g3
BINS=$(addprefix bin/, $(filter-out lib%, $(basename $(patsubst src/%,%,$(wildcard src/*.c)))))

run: all
	@./run.sh
all: $(BINS) lib/libmemalloc.so
bin/%: src/%.c
	$(CC) $(CFLAGS) $< -o $@
lib/%.so: $(wildcard src/lib*.c) $(wildcard src/lib*.h)
	$(CC) $(CFLAGS) -fPIC -shared $< -o $@
clean:
	rm -f obj/* lib/* bin/*
